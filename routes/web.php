<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('center', function () {
    return view('center');
});

Route::get('nav', function () {
    return view('nav');
});

Route::get('media', function () {
    return view('media');
});

Route::get('flex-direction', function () {
    return view('flexDirection');
});

Route::get('grid', function () {
    return view('grid');
});

Route::get('holy-grail', function () {
    return view('holyGrail');
});

Route::get('nav-two', function () {
    return view('navTwo');
});

Route::get('typed-text-illusion', function () {
    return view('typedTextIllusion');
});
