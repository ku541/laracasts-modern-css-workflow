<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Typed Text Illusion</title>
    <style>
        html, body {
            height: 100%;
        }

        body {
            align-items: center;
            display: flex;
            flex-direction: column;
            font-family: sans-serif;
            justify-content: center;
        }

        h1 {
            animation: type 2s steps(28), blink 1s infinite alternate;
            border-right: 3px solid;
            overflow: hidden;
            font-family: monospace;
            white-space: nowrap;
            width: 28ch;
        }

        @keyframes type {
            from {
                width: 0;
            }
        }

        @keyframes blink {
            50% {
                border-color: transparent;
            }
        }

        .fadeIn {
            animation: fadeIn 1s;
            animation-fill-mode: both;
        }

        .wait-2s {
            animation-delay: 2s;
        }

        .wait-3s {
            animation-delay: 3s;
        }

        @keyframes fadeIn {
            from { opacity: 0; }
            to { opacity: 1; }
        }
    </style>
</head>

<body>
    <h1>I Will Sign Up For Laracasts</h1>

    <p class="fadeIn wait-2s">Bla bla bla just sign up mmkay?</p>

    <p class="fadeIn wait-3s"><button>Sign Up</button></p>
</body>

</html>
