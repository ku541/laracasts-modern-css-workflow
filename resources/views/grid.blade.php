<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Grid</title>
    <style>
        .grid__row {
            display: flex;
            flex-wrap: wrap;
            margin-bottom: 2em;
        }

        .grid__column {
            border: 1px solid gray;
            flex: 1;
            text-align: center;
        }

        .aside {
            flex-basis: 150px;
        }

        .primary {
            flex-basis: 500px;
        }
    </style>
</head>

<body>
    <div class="grid">
        <div class="grid__row">
            <div class="grid__column">1</div>
            <div class="grid__column">2</div>
            <div class="grid__column">3</div>
            <div class="grid__column">4</div>
        </div>

        <div class="grid__row">
            <div class="grid__column">1</div>
            <div class="grid__column">2</div>
        </div>

        <div class="grid__row">
            <div class="grid__column">1</div>
            <div class="grid__column">2</div>
            <div class="grid__column">3</div>
            <div class="grid__column">4</div>
            <div class="grid__column">5</div>
            <div class="grid__column">6</div>
            <div class="grid__column">7</div>
            <div class="grid__column">8</div>
        </div>

        <div class="grid__row">
            <div class="grid__column aside">sidebar</div>
            <div class="grid__column primary">main</div>
        </div>
    </div>
</body>

</html>
