<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Holy Grail Layout</title>
    <style>
        /* Setup */

        header {
            background: red;
        }

        h1 {
            margin: 0;
        }

        aside {
            background: yellow;
        }

        main {
            background: blue;
        }

        footer {
            background: pink;
        }

        /* Flexbox */

        .container {
            display: flex;
            flex-wrap: wrap;
        }

        header,
        footer {
            flex-basis: 100%;
        }

        aside {
            flex-basis: 175px;
        }

        main {
            flex: 1;
        }

        @media screen and (max-width: 520px) {
            .container>* {
                flex-basis: 100%;
            }
        }
    </style>
</head>

<body>
    <div class="container">
        <header>
            <h1>My Site</h1>
        </header>

        <aside>
            <ul>
                <li><a href="#">Some Link</a></li>
                <li><a href="#">Some Link</a></li>
                <li><a href="#">Some Link</a></li>
                <li><a href="#">Some Link</a></li>
                <li><a href="#">Some Link</a></li>
                <li><a href="#">Some Link</a></li>
                <li><a href="#">Some Link</a></li>
                <li><a href="#">Some Link</a></li>
                <li><a href="#">Some Link</a></li>
                <li><a href="#">Some Link</a></li>
            </ul>
        </aside>

        <main>main content</main>

        <footer>footer</footer>
    </div>
</body>

</html>
