<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Flex Direction</title>
    <style>
        .container {
            display: flex;
            flex-direction: row-reverse;
        }

        .box {
            align-items: center;
            background: red;
            display: flex;
            flex: 1;;
            height: 200px;
            justify-content: center;
            margin-bottom: 1em;
            margin-right: 1em;
            width: 200px;
        }

        .box:nth-child(2) {
            flex: 3;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="box">My box 1</div>
        <div class="box">My box 2</div>
        <div class="box">My box 3</div>
    </div>
</body>

</html>
