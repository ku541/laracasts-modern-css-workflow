<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Media Component</title>
    <style>
        .Media {
            background: white;
            display: flex;
            padding: 20px;
        }

        .Media__figure {
            margin-right: 1em;
        }

        .Media__body p:first-child {
            margin-top: 0;
        }
    </style>
</head>

<body>
    <div class="Media">
        <div class="Media__figure">
            <img src="https://www.gravatar.com/avatar">
        </div>

        <div class="Media__body">
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum possimus magnam dicta soluta eveniet
                doloremque quisquam perspiciatis natus ad nesciunt! Modi saepe beatae, tempore ab repudiandae recusandae
                dignissimos aliquid amet.</p>

            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quia perspiciatis, consequuntur ipsa velit
                nihil veniam numquam repudiandae minus error totam sed non officia quas consequatur nam ipsum repellat?
                Earum, ipsam!</p>

            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum possimus magnam dicta soluta eveniet
                doloremque quisquam perspiciatis natus ad nesciunt! Modi saepe beatae, tempore ab repudiandae recusandae
                dignissimos aliquid amet.</p>

            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quia perspiciatis, consequuntur ipsa velit
                nihil veniam numquam repudiandae minus error totam sed non officia quas consequatur nam ipsum repellat?
                Earum, ipsam!</p>
        </div>
    </div>
</body>

</html>
