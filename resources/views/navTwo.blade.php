<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Nav With Search</title>
    <style>
        /* Setup */

        body {
            font-family: sans-serif;
        }

        ul,
        li {
            list-style: none;
            margin: 0;
            padding: 0;
        }

        li {
            margin: 20px 0;
        }

        a {
            color: white;
            text-decoration: none;
            display: inline-block;
        }

        a:hover {
            text-decoration: underline;
        }

        /* Flexbox */

        nav>ul {
            align-items: center;
            background: #292929;
            display: flex;
            justify-content: space-around;
        }

        @media screen and (max-width: 520px) {
            nav>ul {
                flex-direction: column;
            }
        }
    </style>
</head>

<body>
    <nav>
        <ul>
            <li><a href="#">Link</a></li>
            <li><a href="#">Link</a></li>
            <li><a href="#">Link</a></li>
            <li>
                <form action="">
                    <input type="search">
                </form>
            </li>
        </ul>
    </nav>
</body>

</html>
